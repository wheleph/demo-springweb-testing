package io.gitlab.wheleph.demo.springweb_testing;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockServletContext;

import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

class UserNameInterceptorRawTest {

    private final UserNameInterceptor userNameInterceptor = new UserNameInterceptor(Map.of("t1", "Alex"));

    @Test
    public void testKnownUser() throws Exception {
        MockHttpServletRequest req = get("/whatever")
                .param(UserNameInterceptor.REQ_PARAM_USER_TOKEN, "t1")
                .buildRequest(new MockServletContext());
        MockHttpServletResponse resp = new MockHttpServletResponse();

        userNameInterceptor.preHandle(req, resp, "doesn't matter");

        Assertions.assertEquals(200, resp.getStatus());
    }

    @Test
    public void testUnknownUser() throws Exception {
        MockHttpServletRequest req = get("/whatever")
                .param(UserNameInterceptor.REQ_PARAM_USER_TOKEN, "t4")
                .buildRequest(new MockServletContext());
        MockHttpServletResponse resp = new MockHttpServletResponse();

        userNameInterceptor.preHandle(req, resp, "doesn't matter");

        Assertions.assertEquals(403, resp.getStatus());
    }

    @Test
    public void testMissingParam() throws Exception {
        MockHttpServletRequest req = get("/whatever")
                .buildRequest(new MockServletContext());
        MockHttpServletResponse resp = new MockHttpServletResponse();

        userNameInterceptor.preHandle(req, resp, "doesn't matter");

        Assertions.assertEquals(403, resp.getStatus());
    }
}