package io.gitlab.wheleph.demo.springweb_testing;

import io.gitlab.wheleph.demo.springweb_testing.test_util.PingController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.lang.NonNull;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(PingController.class)
class UserNameInterceptorTest {
    @Autowired
    private MockMvc mockMvc;

    @TestConfiguration
    static class TestConfig {
        @Bean
        public WebMvcConfigurer testWebMvcConfigurer() {
            return new WebMvcConfigurer() {
                @Override
                public void addInterceptors(@NonNull InterceptorRegistry registry) {
                    String apiEndpointPattern = "/**";

                    Map<String, String> users = Map.of("t1", "Alex");

                    registry.addInterceptor(new UserNameInterceptor(users))
                            .addPathPatterns(apiEndpointPattern)
                            .order(0);
                }
            };
        }
    }

    @Test
    public void testKnownUser() throws Exception {
        MockHttpServletRequestBuilder req = get("/ping")
                .param(UserNameInterceptor.REQ_PARAM_USER_TOKEN, "t1");

        mockMvc.perform(req)
                .andExpect(status().isOk())
                .andExpect(request().attribute(HelloController.REQ_ATTR_USER_NAME, "Alex"));
    }

    @Test
    public void testUnknownUser() throws Exception {
        MockHttpServletRequestBuilder req = get("/ping")
                .param(UserNameInterceptor.REQ_PARAM_USER_TOKEN, "t4");

        mockMvc.perform(req)
                .andExpect(status().isForbidden());
    }

    @Test
    public void testMissingParam() throws Exception {
        MockHttpServletRequestBuilder req = get("/ping");

        mockMvc.perform(req)
                .andExpect(status().isForbidden());
    }
}