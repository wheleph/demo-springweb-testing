package io.gitlab.wheleph.demo.springweb_testing;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(HelloController.class)
class HelloControllerEndToEndTest {
    @Autowired
    private MockMvc mockMvc;

    @TestConfiguration
    static class TestConfig {
        @Bean
        public WebMvcConfigurer actualWebMvcConfigurer() {
            return new WebConfigInterceptors();
        }
    }

    @Test
    void testHello() throws Exception {
        MockHttpServletRequestBuilder req = get("/hello")
                .param(UserNameInterceptor.REQ_PARAM_USER_TOKEN, "t1");

        mockMvc.perform(req)
                .andExpect(status().isOk())
                .andExpect(content().string("Hello Alex"));
    }
}