package io.gitlab.wheleph.demo.springweb_testing;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(HelloController.class)
class HelloControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void testHello() throws Exception {
        MockHttpServletRequestBuilder req = get("/hello")
                .requestAttr(HelloController.REQ_ATTR_USER_NAME, "Max");

        mockMvc.perform(req)
                .andExpect(status().isOk())
                .andExpect(content().string("Hello Max"));
    }

}