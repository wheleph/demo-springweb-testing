package io.gitlab.wheleph.demo.springweb_testing;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig {

    /**
     * This configurer is a regular @Bean within @Configuration and hence it is NOT used for @WebMvcTest tests.
     * This allows to add/remove HandlerInterceptors in those test as needed
     * to be able to test web components (HandlerInterceptors and Controllers) in isolation.
     *
     * More details: https://docs.spring.io/spring-boot/docs/2.4.2/reference/htmlsingle/#boot-features-testing-spring-boot-applications-testing-user-configuration
     */
    @Bean
    public WebMvcConfigurer webMvcConfigurerInterceptors() {
        return new WebConfigInterceptors();
    }
}
