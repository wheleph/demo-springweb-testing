package io.gitlab.wheleph.demo.springweb_testing;

import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Map;

public class WebConfigInterceptors implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        String endpointPattern = "/**";

        Map<String, String> users = Map.of("t1", "Alex",
                "t2", "Bob",
                "t3", "Carol");

        registry.addInterceptor(new UserNameInterceptor(users))
                .addPathPatterns(endpointPattern)
                .order(0);
    }
}
