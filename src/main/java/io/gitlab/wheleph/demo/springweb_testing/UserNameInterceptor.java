package io.gitlab.wheleph.demo.springweb_testing;

import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

import static org.springframework.http.HttpStatus.FORBIDDEN;

public class UserNameInterceptor implements HandlerInterceptor {
    static final String REQ_PARAM_USER_TOKEN = "token";

    private final Map<String, String> users;

    public UserNameInterceptor(Map<String, String> users) {
        this.users = users;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws IOException {
        String userToken = request.getParameter(REQ_PARAM_USER_TOKEN);

        if (!StringUtils.hasText(userToken) || !users.containsKey(userToken)) {
            response.sendError(FORBIDDEN.value(), "Unknown user token");
            return false;
        }

        String userName = users.get(userToken);
        request.setAttribute(HelloController.REQ_ATTR_USER_NAME, userName);
        return true;
    }
}
