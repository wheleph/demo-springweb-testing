package io.gitlab.wheleph.demo.springweb_testing;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    static final String REQ_ATTR_USER_NAME = "userName";

    @GetMapping("/hello")
    public String sayHello(@RequestAttribute(REQ_ATTR_USER_NAME) String userName) {
        return "Hello " + userName;
    }

}
